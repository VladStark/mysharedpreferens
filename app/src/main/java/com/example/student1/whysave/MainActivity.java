package com.example.student1.whysave;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button bSave,bLoad;
    EditText etInput;
    SharedPreferences preferences;
    static final String KEY_PREF = "save";
    private  static final String MY_DATA = "data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        bLoad = (Button) findViewById(R.id.b_load);
        bSave = (Button) findViewById(R.id.b_save);
        etInput = (EditText) findViewById(R.id.et_input);

        bSave.setOnClickListener(this);
        bLoad.setOnClickListener(this);

        preferences = getSharedPreferences(KEY_PREF,MODE_PRIVATE);
    }

    public  void onClick(View view) {
        SharedPreferences.Editor editor = preferences.edit();
        switch (view.getId()) {
            case R.id.b_save :
                editor.putString(MY_DATA,etInput.getText().toString());
                editor.commit();
                editor.apply();
                break;

            case R.id.b_load :
                String loadedData = preferences.getString(MY_DATA, "");
                etInput.setText(loadedData);
                break;

        }
    }
}

